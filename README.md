<!-- TOC -->

- [关于](#关于)
  - [项目用途](#项目用途)
    - [下一步](#下一步)
- [版本说明](#版本说明)
  - [2021](#2021)
    - [9.30](#930)
    - [8.10](#810)
    - [8.5](#85)
    - [7.19](#719)
    - [7.6](#76)
    - [7.1](#71)

<!-- /TOC -->
# 关于

## 项目用途

> 基于graphic-object项目的Elasearch图形化显示项目.

### 下一步

0. 对当前查询条件进行命名保存

# 版本说明

## 2021

### 9.30

模块|类型|调整内容
-|-|-
GraphicObject|更新|升级至9.30版本
package.json|增加|nodemon-webpack-plugin
lib/search.mjs|调整|Graphic.bootstrap5.formTable -> Graphic.bootstrap5.esSearchTable
### 8.10

模块|类型|调整内容
-|-|-
lib/AscDesc|新增|新增索引查询排序功能
lib/Result|调整|默认结果集渲染改为正序
GraphicObject|更新|升级至8.10版本

### 8.5

模块|调整内容
-|-
GraphicObject|升级至8.5版本

### 7.19

模块|调整内容
-|-
GraphicObject|升级至7.19版本

### 7.6

模块|调整内容
-|-
GraphicObject|升级至7.6版本
IndexButton|当前索引进行颜色区分
EsConfig&Mapping|生产模式下进行隐藏,可通过设置为开发模式(mode: 'development')<br>或在页面执行:localStorage.setItem('开发模式', 1)

### 7.1

0. 原有GraphicES项目衍生出Webpack框架下GraphicObjectES项目.
0. NPM包发布