// import '/Users/ff4c00/Documents/Space/知识体系/应用科学/计算机科学/理论计算机科学/编程语言理论/Javascript/graphic-object'
import 'graphic-object'

import AssemblyLine from './core/assemblyLine.mjs'
import Elasticsearch from './core/elasticsearch.mjs'

import esConfig from'./lib/esConfig.mjs'
import indexButton from'./lib/indexButton.mjs'
import search from'./lib/search.mjs'
import ascDesc from'./lib/ascDesc.mjs'
import aggs from'./lib/aggs.mjs'
import aggsChart from'./lib/aggsChart.mjs'
import result from'./lib/result.mjs'
import mapping from'./lib/mapping.mjs'

window.GraphicES ||= new Object();

class Index {
  constructor(params = {}) {
    this.elasticsearch = new Elasticsearch(params)
    let defaultIndex = new Elasticsearch()
    let handler = {
      get: function (target, name) {
        return target.hasOwnProperty(name) ? target[name] : defaultIndex;
      }
    }
    this.elasticsearch = new Proxy(this.elasticsearch, handler)

    let defaultValue = {
      unineCode: undefined, // 当前索引唯一标识
      configRequest: this.elasticsearch[params.unineCode].配置.请求, // 当前请求参数配置
      urlParams: (new URLSearchParams(window.location.search)), // url参数
      父节点: document.getElementById('space'),
      dataStore: {
        indexResult: {},
        indexResponse: {},
        exportExcel: {},
      }, // 数据集合 后面交由流水线(AssemblyLine)控制
      生产模式: (process.env.NODE_ENV == 'production'),
      开发模式: (process.env.NODE_ENV == 'development'),
    }
    params = Object.assign(defaultValue, params)
    for (let [key, value] of Object.entries(params)) { this[key] = value }

    this._init()
  }

  async _init() {
    this.assemblyLine = new AssemblyLine({
      links: ['init', 'load', 'exportExcel'],
      nodes: [esConfig, indexButton, search, ascDesc, result, aggs, aggsChart, mapping],
      treasure: this,
      currentId: function(){return this.treasure.unineCode},
    })

    this.assemblyLine.validate().then(result => {
      if (!result[0]) return;
      result[1].init()
    })
  }

  beforeLoad() {
    let index = this.elasticsearch[this.unineCode]

    this.assemblyLine.nodesObject.forEach(item => {
      if (!!!item.element) return
      item.element.childNodes.forEach(node => {
        node.style.display = 'none'
      })
    })

    this.currentIndex = index
    this.configRequest = this.currentIndex.配置.请求
    return [true, this]
  }

  async load() {
    if (!!!this.dataStore.indexResult[this.unineCode]) {
      await this.currentIndex.indexSearch(this.requestParams).then(result => {
        if (result[0]) {
          this.dataStore.indexResult[this.unineCode] = result[1]
          this.dataStore.indexResponse[this.unineCode] = result[2]
        } else {
          this.dataStore.indexResult[this.unineCode] = [{ 异常信息: result[1] }]
        }
      })
    }
    return [true, '']
  }

  change(params) {
    if (!!params.unineCode) {
      this.unineCode = params.unineCode
    } else {
      if (!!!this.unineCode) return [false, '当前索引为空'];
    }

    if (!!params.reload) {
      this.dataStore.indexResult[this.unineCode] = undefined
      this.dataStore.indexResponse[this.unineCode] = undefined
    }
    return this.assemblyLine.load()
  }

  beforeExportExcel() {
    this.dataStore['exportExcel'] = {}
  }

  exportExcel() {
    if (Object.keys(this.dataStore['exportExcel']).length == 0) return [false, '数据为空']
    let fileName = this.currentIndex.其他.索引描述 + '数据导出'
    return Graphic.excel.toXlsx({ 渲染对象: this.dataStore['exportExcel'], 文件名: fileName })
  }

}

window.GraphicES.Index = Index