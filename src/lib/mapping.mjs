export default class Mapping {
  constructor(params = {}) {
    let res = Graphic.toElement({ element: 'div', id: 'space-mapping' })
    if (!res[0]) return res
    params.父节点.appendChild(res[1])
    this.element = res[1]
    return [true, this]
  }

  afterLoad(params) {
    if (!!!params.currentIndex.索引映射) return [false, '当前索引的字段映射为空']

    let 索引映射 = Object.assign({}, params.currentIndex.索引映射)
    for (let [key, value] of Object.entries(索引映射)) { 索引映射[key] = JSON.stringify(value) }

    let res = Graphic.bootstrap5.baseTable({
      元素ID: this.element.id + '-' + params.unineCode,
      渲染对象: 索引映射,
      每行元素个数: 2,
      父元素: this.element,
      标题: params.currentIndex.其他.索引描述 + "索引映射信息"
    })

    // TODO: 后期优化 统一调用方法
    if(params.生产模式 && (localStorage.getItem('开发模式') != '1')){
      this.element.style.display = 'none'
      return [false, "当前为生产模式, 参数表单不做展示, 进入开发模式: localStorage.setItem('开发模式', 1)"]
    }

    return res
  }
}